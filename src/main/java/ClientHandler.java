import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientHandler extends Thread {
    private Socket socketForClient;
    private BufferedReader fromClient;
    private PrintWriter toClient;


    public ClientHandler(Socket clientSocket){
        socketForClient=clientSocket;
    }


    @Override
    public void run(){
        try {
            toClient = new PrintWriter(socketForClient.getOutputStream(), true);
            fromClient = new BufferedReader(new InputStreamReader(socketForClient.getInputStream()));

            String message;

            while ((message = fromClient.readLine()) != null) {
                System.out.println("From client holder");
                if(message.equalsIgnoreCase("guest"))
                {
                    System.out.println("Guest come: "+message);
                    toClient.println("Guest come: "+message);
                    new GuestHandler(socketForClient).start();
                    break;
                }
                if(message.equalsIgnoreCase("tourist"))
                {
                    System.out.println("Tourist come: "+message);
                    toClient.println("Tourist come: "+message);
                    new TouristHandler(socketForClient).start();
                    break;

                }
                if (".".equals(message)) {
                    toClient.println("bye");
                    break;
                }
                //toClient.println(message);
            }

           // socketForClient.close();
           // toClient.close();
           // fromClient.close();

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
