import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

public class AirbnbServer {

    private static CopyOnWriteArrayList<City> cities;


    private void generateCityOffer(){
        cities=new CopyOnWriteArrayList<>();
        cities.add(new City("Bucuresti"));
        cities.add(new City("Brasov"));
        cities.add(new City("Cluj-Napoca"));
    }


    public static String addLocationToCity(String location, String city){
        for(City c:cities){
            if(c.getName().equalsIgnoreCase(city)) {
                c.addLocation(location);
                return "Location "+location+" added to city "+city;
            }
        }
        return "City not found! "+city;
    }

    public static String listAllCities(){
        String s="";
        for(City c:cities) {
            s+=c.getName()+" ";
        }
        System.out.println(s);
        return s;
    }

    public static String getLocationsfromCity(String city){
        for(City c:cities){
            if(c.getName().equalsIgnoreCase(city)) {
                return c.getLocationsNames();
            }
        }
        return "City has no locations registered! "+city;
    }

    public static String makeReservationToLocation(String city, String location, int month, int nrMonth){
        for(City c:cities){
            if(c.getName().equalsIgnoreCase(city)) {
                String locations= c.getLocationsNames();
                if(locations.contains(location)){
                    if(c.makeReservation(location,month,nrMonth))
                        return "Reservation done "+city+" "+location;
                    else
                        return "Period requested not available in "+city+" "+location;
                }
                else{
                    return "City "+city+" has no location "+location;
                }
            }
        }

        return "City not found! "+city;
    }


    public void start(){
        generateCityOffer();
        ServerSocket AirbnbChannel = null;
        try {
            AirbnbChannel = new ServerSocket(50);
            while (true)
            {
                Socket socketForClient = AirbnbChannel.accept();
                new ClientHandler(socketForClient).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static void main(String[] args){

        AirbnbServer airbnbServer=new AirbnbServer();
        airbnbServer.start();


    }
}
