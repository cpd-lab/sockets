import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

public class City {
    private String name;
    private CopyOnWriteArrayList<Location> locations;


    public City(String name){
        this.name=name;
        locations=new CopyOnWriteArrayList<Location>();
    }

    void addLocation(String name){
        locations.add(new Location(name));
    }

    boolean makeReservation(String locationName, int startMonth, int nrMonths){
        Location wantedLocation=getLocation(locationName);
        if(wantedLocation!=null)
            return wantedLocation.makeReservation(startMonth,nrMonths);
        return false;
    }

    private Location getLocation(String name) {
        for (Location i:locations) {
            if(name.equals(i.getName()))
                return i;
        }
        return null;
    }

    String getLocationsNames(){
        String locationName="";
        for (Location l:locations) {
            locationName+=l.getName()+" ";
        }
        return locationName;
    }

    private CopyOnWriteArrayList<Location> getLocations(){
        return locations;
    }

    private boolean checkAvailability(String locationName, int month, int nrMonths){
        Location wantedLocation=getLocation(locationName);
        return wantedLocation.checkAvailability(month,nrMonths);
    }

    String getName(){
        return name;
    }
}
