import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class GuestHandler extends Thread{

    private Socket socketForClient;
    private BufferedReader fromClient;
    private PrintWriter toClient;

    public GuestHandler(Socket socket){
        this.socketForClient=socket;
    }

    @Override
    public void run(){
        System.out.println("Thread for guest just started!"+getId());
        try {
            toClient = new PrintWriter(socketForClient.getOutputStream(), true);
            fromClient = new BufferedReader(new InputStreamReader(socketForClient.getInputStream()));


            String message;

            while ((message = fromClient.readLine()) != null) {
                if (".".equals(message)) {
                    toClient.println("bye");
                    break;
                }

                if(message.startsWith("addLocationToCity ")){
                    String[] input=message.split(" ");
                    String response=AirbnbServer.addLocationToCity(input[1],input[2]);
                    System.out.println(response);
                    toClient.println(response);
                }
                //TODO implement the message parsing here
                System.out.println("From guest: "+message);
                //toClient.println("From guest: "+message);
            }

            socketForClient.close();

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }


}
