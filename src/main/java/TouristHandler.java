import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class TouristHandler extends Thread{
    private Socket socketForClient;
    private BufferedReader fromClient;
    private PrintWriter toClient;

    public TouristHandler(Socket socket){
        this.socketForClient=socket;
    }

    @Override
    public void run(){
        System.out.println("Thread for tourist just started!"+getId());
        try {
            toClient = new PrintWriter(socketForClient.getOutputStream(), true);
            fromClient = new BufferedReader(new InputStreamReader(socketForClient.getInputStream()));

            String message;

            while ((message = fromClient.readLine()) != null) {
                if("listAllCities".equalsIgnoreCase(message)){
                    toClient.println(AirbnbServer.listAllCities());
                }
                if(message.startsWith("getLocationsfromCity")){
                    String[] input=message.split(" ");
                    String response=AirbnbServer.getLocationsfromCity(input[1]);
                    System.out.println(response);
                    toClient.println(response);
                }if(message.startsWith("makeReservationToLocation")){
                    String[] input=message.split(" ");
                    String response=AirbnbServer.makeReservationToLocation(input[1],input[2],Integer.parseInt(input[3]),Integer.parseInt(input[4]));
                    System.out.println(response);
                    toClient.println(response);
                }

                if (".".equals(message)) {
                    toClient.println("bye");
                    break;
                }
                //TODO implement the message parsing here
                System.out.println("From tourist: "+message);
                //toClient.println("From tourist: "+message);
            }

            socketForClient.close();

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
