import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Tourist {
    Socket socket;
    private BufferedReader fromServer;
    private PrintWriter toServer;

    public void start(String ip, int port) throws IOException {
        socket = new Socket(ip, port);
        fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        toServer = new PrintWriter(socket.getOutputStream(), true);
    }

    public String announceYourself(String whoYouAre) throws IOException {
        toServer.println(whoYouAre);
        return fromServer.readLine();
    }

    public String getLocationsfromCity(String city) throws IOException {
        String message="getLocationsfromCity "+city;
        toServer.println(message);
        return fromServer.readLine();
    }

    public String makeReservationToLocation(String city, String location, int month, int nrMonth) throws IOException {
        String message="makeReservationToLocation "+city+" "+location+" "+month+" "+nrMonth;
        toServer.println(message);
        return fromServer.readLine();
    }

    public String listAllCities() throws IOException {
        String message="listAllCities";
        toServer.println(message);
        return fromServer.readLine();
    }

    public void stop() throws IOException {
        toServer.close();
        fromServer.close();
        socket.close();
    }

}
