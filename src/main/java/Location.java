import java.util.ArrayList;
import java.util.HashMap;

public class Location {
    private String name;
    private HashMap<Integer,Boolean> monthAvailable;

    Location(String name){
        this.name=name;
        monthAvailable=new HashMap<Integer,Boolean>();
        for(int i=1;i<=12;i++)
            monthAvailable.put(i,true);
    }

    boolean makeReservation(int month, int nrMonths){
        if(checkAvailability(month,nrMonths)){
            for (int i=month;i<month+nrMonths;i++) {
                //System.out.println("key: " + i + " value: " + monthAvailable.get(i));
                if(monthAvailable.get(i))
                    monthAvailable.replace(i,false);
            }
            return true;
        }
        else
            return false;
    }

     ArrayList<Integer> getFreeMonths() {
        ArrayList<Integer> freeMonths = new ArrayList<Integer>();
        for (int i = 1; i <= 12; i++) {
            if (monthAvailable.get(i))
                freeMonths.add(i);
        }
        return freeMonths;
    }

    String getName(){
        return name;
    }

    boolean checkAvailability(int month, int nrMonths)
    {
        int nrFree=0;
        for (int i=month;i<month+nrMonths;i++) {
            //System.out.println("key: " + i + " value: " + monthAvailable.get(i));
            if(monthAvailable.get(i))
            {
                nrFree++;
            }
            else break;
        }
        if(nrFree<nrMonths)
            return false;
        else
            return true;
    }
}
