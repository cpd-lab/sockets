import java.util.HashMap;

//pentru fiecare locatie din fiecare oras se tine o evidenta a rezervarilor, atat cele reusite cat si cele respinse
public class MyEvidenceStruct {

    private String city;
    private String location;
    private HashMap<Integer,Boolean> reservations; //id-ul clientului care a vrut sa faca rezervare si daca a reusit sau nu sa o faca


    public MyEvidenceStruct(String c, String  l)
    {
        city=c;
        location=l;
        reservations=new HashMap<>();
    }

    public int getNrOfReservationsDone(){
        int nr=0;
        if(reservations.size()>0)
        for (Integer key: reservations.keySet()) {
            if(reservations.get(key))
               nr++;
        }
        else return -1;
        return nr;
    }

    public void addReservation(int id, boolean result){
        reservations.put(id,result);
    }

    public String getCity(){return city;}
    public String getLocation(){return location;}

    public String getTouristWhoReserved(){
        String tourists="";
        for (Integer key: reservations.keySet()) {
            if(reservations.get(key))
                tourists+= key.toString()+" ";
        }
        return tourists;
    }

}
