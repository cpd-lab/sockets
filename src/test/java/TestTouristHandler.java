import java.io.IOException;
import java.util.Calendar;

public class TestTouristHandler extends Thread{

    private String city;
    private String location;
    private int month;
    private int nrMonth;
    private int id;
    private int minute;
    private int second;

    public TestTouristHandler(String city, String location, int month, int nrMonths,int id,int minute,int second){
        this.city=city;
        this.location=location;
        this.month=month;
        this.nrMonth=nrMonths;
        this.id=id;
        this.minute=minute;
        this.second=second;
    }


    @Override
    public void run() {
        Tourist tourist = new Tourist();
        try {
            tourist.start("127.0.0.1", 50);
            String response = tourist.announceYourself("tourist");
            System.out.println(response+" "+id);

           while(true)
            {
                Calendar calendar1 = Calendar.getInstance();
                if(calendar1.get(Calendar.MINUTE)==this.minute && calendar1.get(Calendar.SECOND)==this.second )
                {
                    String responseGetLocationsFromCity=tourist.getLocationsfromCity(city);
                    System.out.println("Locations in "+city+" :"+responseGetLocationsFromCity+"from :"+id);
                    String responseMakeReservation=tourist.makeReservationToLocation(city,location,month,nrMonth);
                    System.out.println(responseMakeReservation+" "+id);
                    if(responseMakeReservation.startsWith("Reservation")) //Reservation done
                    {
                        AirbnbTest.addStruct(city,location,id,true);
                    }
                    if(responseMakeReservation.startsWith("Period"))//Period requested not available in
                    {
                        AirbnbTest.addStruct(city, location, id, false);
                    }
                    break;
                }
            }
            tourist.stop();
        } catch (IOException e) {
            e.printStackTrace();
        }



    }
}
