import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.Assert.assertEquals;

public class AirbnbTest {


    public static CopyOnWriteArrayList<MyEvidenceStruct> possibleReservations;


    public static MyEvidenceStruct getStruct(String city, String location){
        for(int i=0;i<possibleReservations.size();i++)
        {
            if(possibleReservations.get(i).getLocation().equalsIgnoreCase(location) &&
                    possibleReservations.get(i).getCity().equalsIgnoreCase(city)){
                return possibleReservations.get(i);
            }
        }
        return null;
    }
    public static void addStruct(String city, String location, int id, boolean result){
        MyEvidenceStruct currentStruct= getStruct(city, location);
        if(currentStruct!=null)
            currentStruct.addReservation(id,result);

    }

    @Test
    public void announce() throws IOException, InterruptedException {
        possibleReservations=new CopyOnWriteArrayList<>();


        //  GUEST
        Guest guest=new Guest();
        guest.start("127.0.0.1", 50);

        String response1=guest.announceYourself("Guest");
        System.out.println(response1);
        assertEquals("Guest come: Guest", response1);



        // GUEST1
        Guest guest1=new Guest();
        guest1.start("127.0.0.1", 50);
        System.out.println(guest1.announceYourself("Guest"));


        // Add locations to cities
        String responseAddLocation=guest.addLocationToCity("Tractorul","Brasov");
        System.out.println(responseAddLocation);
        assertEquals("Location Tractorul added to city Brasov",responseAddLocation);
        System.out.println(guest.addLocationToCity("Republicii","Brasov"));

        System.out.println(guest1.addLocationToCity("Tampa","Brasov"));
        System.out.println(guest1.addLocationToCity("Schei","Brasov"));

        possibleReservations.add(new MyEvidenceStruct("Brasov","Tractorul"));
        possibleReservations.add(new MyEvidenceStruct("Brasov","Republicii"));
        possibleReservations.add(new MyEvidenceStruct("Brasov","Tampa"));
        possibleReservations.add(new MyEvidenceStruct("Brasov","Schei"));


        System.out.println(guest.addLocationToCity("Baneasa","Bucuresti"));
        System.out.println(guest1.addLocationToCity("Centru","Bucuresti"));
        System.out.println(guest.addLocationToCity("Pipera","Bucuresti"));
        System.out.println(guest1.addLocationToCity("Ferentari","Bucuresti"));

        possibleReservations.add(new MyEvidenceStruct("Bucuresti","Baneasa"));
        possibleReservations.add(new MyEvidenceStruct("Bucuresti","Centru"));
        possibleReservations.add(new MyEvidenceStruct("Bucuresti","Pipera"));
        possibleReservations.add(new MyEvidenceStruct("Bucuresti","Ferentari"));



        System.out.println(guest.addLocationToCity("Marasti","Cluj-Napoca"));
        System.out.println(guest1.addLocationToCity("Zorilor","Cluj-Napoca"));
        System.out.println(guest.addLocationToCity("Centru","Cluj-Napoca"));
        System.out.println(guest1.addLocationToCity("Grigorescu","Cluj-Napoca"));

        possibleReservations.add(new MyEvidenceStruct("Cluj-Napoca","Marasti"));
        possibleReservations.add(new MyEvidenceStruct("Cluj-Napoca","Zorilor"));
        possibleReservations.add(new MyEvidenceStruct("Cluj-Napoca","Centru"));
        possibleReservations.add(new MyEvidenceStruct("Cluj-Napoca","Grigorescu"));




        //Make reservations in Brasov
     /*   String responseMakeReservation=tourist.makeReservationToLocation("Brasov","Tractorul",1,6);
        System.out.println(responseMakeReservation);
        assertEquals("Reservation done Brasov Tractorul",responseMakeReservation);

        String responseMakeReservation1=tourist1.makeReservationToLocation("Brasov","Tractorul",2,2);
        System.out.println(responseMakeReservation1);
        assertEquals("Period requested not available in Brasov Tractorul",responseMakeReservation1);

        String responseMakeReservation2=tourist.makeReservationToLocation("Brasov","Tractorul",3,6);
        System.out.println(responseMakeReservation2);
        assertEquals("Period requested not available in Brasov Tractorul",responseMakeReservation2);


        String responseMakeReservation3=tourist.makeReservationToLocation("Brasov","Tampa",4,6);
        System.out.println(responseMakeReservation3);
        assertEquals("Reservation done Brasov Tampa",responseMakeReservation3);

        String responseMakeReservation4=tourist1.makeReservationToLocation("Brasov","Tampa",2,2);
        System.out.println(responseMakeReservation4);
        assertEquals("Reservation done Brasov Tampa",responseMakeReservation4);

        String responseMakeReservation5=tourist.makeReservationToLocation("Brasov","Tampa",3,5);
        System.out.println(responseMakeReservation5);
        assertEquals("Period requested not available in Brasov Tampa",responseMakeReservation5);


        String responseMakeReservation6=tourist.makeReservationToLocation("Brasov","Schei",4,6);
        System.out.println(responseMakeReservation6);
        assertEquals("Reservation done Brasov Schei",responseMakeReservation6);

        String responseMakeReservation7=tourist1.makeReservationToLocation("Brasov","Republicii",2,2);
        System.out.println(responseMakeReservation7);
        assertEquals("Reservation done Brasov Republicii",responseMakeReservation7);

        String responseMakeReservation8=tourist.makeReservationToLocation("Brasov","Republicii",3,5);
        System.out.println(responseMakeReservation8);
        assertEquals("Period requested not available in Brasov Republicii",responseMakeReservation8);


        //Make reservations in Bucuresti
        String responseMakeReservation9=tourist.makeReservationToLocation("Bucuresti","Schei",4,6);
        System.out.println(responseMakeReservation9);
        assertEquals("City Bucuresti has no location Schei",responseMakeReservation9);

        String responseMakeReservation10=tourist1.makeReservationToLocation("Bucuresti","Centru",9,3);
        System.out.println(responseMakeReservation10);
        assertEquals("Reservation done Bucuresti Centru",responseMakeReservation10);

        String responseMakeReservation11=tourist1.makeReservationToLocation("Bucuresti","Baneasa",3,5);
        System.out.println(responseMakeReservation11);
        assertEquals("Reservation done Bucuresti Baneasa",responseMakeReservation11);


        //Make reservations in Cluj-Napoca

        String responseMakeReservation12=tourist.makeReservationToLocation("Cluj-Napoca","Zorilor",4,6);
        System.out.println(responseMakeReservation12);
        assertEquals("Reservation done Cluj-Napoca Zorilor",responseMakeReservation12);

        String responseMakeReservation13=tourist1.makeReservationToLocation("Cluj-Napoca","Zorilor",9,3);
        System.out.println(responseMakeReservation13);
        assertEquals("Period requested not available in Cluj-Napoca Zorilor",responseMakeReservation13);

        String responseMakeReservation14=tourist1.makeReservationToLocation("Cluj-Napoca","Centru",3,5);
        System.out.println(responseMakeReservation14);
        assertEquals("Reservation done Cluj-Napoca Centru",responseMakeReservation14);
      */
        //se seteaza minutul si secunda din viitor cand sa faca toti turistii rezervari
        int minute=40;
        int second=0;

        int minute1=40;
        int second1=3;
        
        //doar o rezervatie poate fi facuta deaorece toate 3 se incadreaza in lunile 1-6
        TestTouristHandler test=new TestTouristHandler("Brasov","Tractorul",1,6,100,minute,second);//luniile 1 2 3 4 5 6
        test.start();
        TestTouristHandler test1=new TestTouristHandler("Brasov","Tractorul",2,3,101,minute,second);//lunile 2 3 4
        test1.start();
        TestTouristHandler test2=new TestTouristHandler("Brasov","Tractorul",3,2,102,minute,second);//lunile 3 4
        test2.start();

        // aici o sa se faca fie 2 rezervari facute de 103 si 104, fie o singura rezervare facuta de 105
        TestTouristHandler test3=new TestTouristHandler("Cluj-Napoca","Zorilor",1,6,103,minute1,second1);//luniile 1 2 3 4 5 6
        test3.start();
        TestTouristHandler test4=new TestTouristHandler("Cluj-Napoca","Zorilor",7,2,104,minute1,second1);//lunile 7 8
        test4.start();
        TestTouristHandler test5=new TestTouristHandler("Cluj-Napoca","Zorilor",5,3,105,minute1,second1);//lunile 5 6 7
        test5.start();


        test.join();
        test1.join();
        test2.join();
        test3.join();
        test4.join();
        test5.join();

        assertEquals(1,getStruct("Brasov","Tractorul").getNrOfReservationsDone());

        System.out.println("Number of reservestions in Brasov, Tractorul: "+
                getStruct("Brasov","Tractorul").getNrOfReservationsDone());

        System.out.println("Number of reservestions in Cluj, Zorilor: "+
                getStruct("Cluj-Napoca","Zorilor").getNrOfReservationsDone()+". Made by "+
                getStruct("Cluj-Napoca","Zorilor").getTouristWhoReserved());


        guest.stop();
        guest1.stop();

    }
}
